import requests
from PIL.Image import open as open_
from PIL.ImageDraw import Draw


class ProofOfMyWorth:

    def __init__(self):
        self.url = 'https://www.proveyourworth.net/level3/'
        self.full_name = "Serhii Tomchuk"
        self.email = "stomchuk34@gmail.com"
        self.image = 'bmw_for_life.jpg'
        self.cv = 'CV.pdf'
        self.code = 'code.py'
        self.hardcore = "\m/_ hardcore _\m/"
        self.text = "Hello, I'm a Python Backend Developer,\n" \
                    "with about a year of experience, I want to\n" \
                    "develop and grow as a professional :)"
        self.request = requests
        self.session = self.request.session()

    def _page(self):
        return self.session.get(url=self.url).text

    def _storage(self, storage=None):
        try:
            if storage is None:
                storage = dict()
                [
                    storage.update(
                        {
                            ''.join(_ for _ in x[0] if _.isalpha() or _.isalnum()):
                                ''.join(__ for __ in x[1] if __.isalpha() or __.isalnum())
                        }
                    ) for x in [
                    i.split("=")
                    for i in self._page().split("form")[1].split() if '=' in i
                ]
                    if x[0] in ['action', 'name', 'value']
                       and ''.join(__ for __ in x[1] if __.isalpha() or __.isalnum()) != "username"
                ]
            return storage
        except Exception as err:
            print(err)

    def _get_payload_url(self, payload=None):
        try:
            if payload is None:
                payload = {}
            payload[self._storage()['name']] = self._storage()['value']
            payload['username'] = self.full_name
            url_ = f"{self.url}{self._storage()['action']}"
            return self.session.get(
                url=url_,
                params=payload
            ).headers['X-Payload-URL']
        except Exception as err:
            print(err)

    def _downloading(self):
        try:
            resp = self.session.get(self._get_payload_url())
            with open(self.image, 'wb') as f:
                f.write(resp.content)
            return resp
        except Exception as err:
            print(err)

    def _draw(self):
        try:
            image = open_(self.image)
            drawing = Draw(image)
            drawing.text(
                (15, 15),
                f"{self.hardcore}\n"
                f"{self.full_name}\n"
                f"{self._storage()['name']}: {self._storage()['value']}\n"
                f"{self.email}",
                (300, 300, 300),
            )
            image.save(self.image, "JPEG")
        except Exception as err:
            print(err)

    def _prepare_data(self):
        try:
            return {
                "files": {
                    'image': open(f"./{self.image}", "rb"),
                    'code': open(f"./{self.code}", "rb"),
                    'resume': open(f"./{self.cv}", "rb")
                },
                "data": {
                    'email': self.email,
                    'name': self.full_name,
                    'aboutme': self.text
                }
            }
        except Exception as err:
            print(err)

    def _push(self):
        try:
            resp_ = self._downloading()
            self._draw()

            resp = self.session.post(
                url=resp_.headers['X-Post-Back-To'],
                files=self._prepare_data()['files'],
                data=self._prepare_data()['data'],
                # cookies=resp_.cookies['PHPSESSID']
            )

            print(resp.url)
            print(resp.status_code)
            print(resp.text)

        except Exception as err:
            print(err)

    def __call__(self, *args, **kwargs):
        self._push()


if __name__ == '__main__':
    ProofOfMyWorth().__call__()
